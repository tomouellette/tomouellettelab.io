<!DOCTYPE html>
<html>
<head>
	<meta name="generator" content="Hugo 0.87.0" />

  
  <meta charset="utf-8">
  
  
  
  <title>Ouellette & Awadalla (2021)</title>
  
  <meta name="description" content="A personal website for Tom W. Ouellette">
  <meta name="author" content="Tom W. Ouellette">

  
  <meta name="viewport" content="width=device-width, initial-scale=1">

  
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Crimson+Pro&display=swap" rel="stylesheet">

  
  <link rel="stylesheet" href="./css/normalize.css">
  <link rel="stylesheet" href="./css/skeleton.css">
<!--   <link rel="stylesheet" href="./css/animate.css/animate.min.css"> -->


  
  <link rel="icon" type="image/png" href="https://tomouellette.github.io/images/favicon.png">

</head>
<body>
<div class="container">
      <div class="row">
        <div class="two-thirds column" style="margin-top: 10%">
          <h3 class="head-fade fadeInDown">Inferring ongoing cancer evolution from single tumour biopsies using synthetic supervised learning</h3>
          <p class="head-fade fadeInDown"><span style="font-weight:normal">Tom W. Ouellette and Philip Awadalla</span></p>
        </div>
      </div>
    </div>

<div class="body-fade fadeInDown">
  <div class="container">
    <div class="row">
        <div class="twelve columns" style="margin-top: 0%">
          <nav id="nav" class="nav">
          
          <a class="button nav-but" href="index.html">
           
           main
          </a> 
          
          <a class="button nav-but" href="supplementary.html">
           
           supplementary
          </a> 
          
          <a class="button nav-but" href="references.html">
           
           references
          </a> 
          
        </nav>
      </div>
    </div>
  </div>
</div>

<div id="content">
  
  <div class="body-fade fadeInDown">
  <div class="container">
    <div class="row">
      <div class="ten columns intro" style="margin-top: 2%">    
        <div id = "about">
        <b style="font-size:1.1em; color: rgba(0, 0, 0, 1)">Supplementary</b>
        <span style="font-size:1em">
        <p><b>Table of Contents</b></p>
        </span>
        <article>          
          
            <!-- Section 1 -->
            <hr style="border-color:rgb(0,0,0,0.2);border-width:1px;">
            I. Description of synthetic tumour generation methods
            <ol>
                <ol>
                  <li>                                    
                  <a href="./supplementary/1.1_pseudo_algorithms.html">Pseudo-algorithms for stochastic simulations and synthetically sampled tumours</a>
                  <details>
                    <ul>
                      <li>Supplementary Figure 1. Comparison of single population genetic statistics and deep learning models for differentiating between positive selection and neutral evolution</li>
                      <li>Supplementary Figure 2. Predicting the number of subclones (0, 1, 2) in 2.8 million synthetic tumours</li>
                      <li>Pseudo-algorithms for stochastic branching process (positive selection), generative sampling process (neutral evolution), and paired synthetic data generation</li>
                    </ul>
                  </details>
                  </li>
                  <li><a href="./supplementary/1.2_simulation_check.html">Checking simulation model specification relative to sequenced patient tumours</a>
                    <details>
                    <ul>
                      <li>Supplementary Figure 3. Evaluating validity of synthetic data generation scheme with respect to real patient data (removal of low frequency variants based on mean sequencing depth)</li>
                      <li>Supplementary Figure 4. Evaluating validity of synthetic data generation scheme with respect to real patient data (removal of low frequency variants based on mean effective coverage)</li>
                      <li>Supplementary Figure 5. Comparison of nearest neighbour search when using mean effective coverage versus mean sequencing depth.</li>
                    </ul>
                    </details>
                  </li>
                </ol>
            </ol>

            <!-- Section 2 -->
            <hr style="border-color:rgb(0,0,0,0.2);border-width:1px;">
            
              II. Deep learning model performance for base evolutionary inference tasks
              <ol>
                <ol>

                  <li><a href="./supplementary/2.1_mc_dropout_estimates.html">Examining probability estimates for detecting selection under varying subclone characteristics</a>
                    <details>
                    <ul>
                      <li>Supplementary Figure 6. Accurately detecting positive selection and subclonality is dependent on the sequencing depth, number of subclonal mutations, and subclone frequency at time of biopsy</li>
                    </ul>
                    </details>
                  </li>

                  <li>
                  <a href="./supplementary/2.2_model_performance.html">Evaluating model performance in 2.8 million simulated tumours</a>
                    <details>
                    <ul>
                      <li>Supplementary Figure 7. Comparison of single population genetic statistics and deep learning models for differentiating between positive selection and neutral evolution</li>
                      <li>Supplementary Figure 8. Predicting the number of subclones (0, 1, 2) in 2.8 million synthetic tumours</li>
                      <li>Supplementary Figure 9. Correlation between true subclone frequency and predicted subclone frequency using synthetic supervised learning (TumE) and a population genetics informed mixture model (MOBSTER)</li>
                      <li>Supplementary Figure 10. Error in predicting frequency of 2 detectable subclones with synthetic supervised learning (TumE)</li>
                      <li>Supplementary Figure 11. Relationship between frequencies of subclones in the 2 subclone setting and the mean percentage error for the highest frequency subclone (1st subclone). </li>
                      <li>Supplementary Figure 12. Relationship between frequencies of subclones in the 2 subclone setting and the mean percentage error for the lowest frequency subclone (2nd subclone). </li>                     
                    </ul>
                    </details>
                  </li>

                  

                  <li>
                  <a href="./supplementary/2.3_mobster_estimates.html">Testing generalizability of selection estimates using an orthogonal cancer evolution simulator</a>
                    <details>
                    <ul>
                      <li>Supplementary Figure 13. Evaluation of TumE evolutionary classification estimates in an orthogonally simulated dataset of 900 synthetic tumours</li>
                      <li>Supplementary Figure 14. Subclone frequency estimates are only accurate at detectable frequency ranges</li>
                    </ul>
                    </details>
                  </li>

                  <li>
                  <a href="./supplementary/2.4_birthdeathrate.html">Impact of variable birth and death rates on estimating the number of subclones and subclone frequency</a>
                    <details>
                    <ul>
                      <li>Supplementary Figure 15. TumE performance (precision and recall) for predicting the number of subclones across 26 different birth rate and death rate combinations in 6.7 million synthetic tumours.</li>
                      <li>Supplementary Figure 16. TumE performance (precision and recall) for predicting the frequency of a single subclone across 26 different birth rate and death rate combinations in 6.7 million synthetic tumours.</li>
                    </ul>
                    </details>
                  </li>

                  <li>
                  <a href="./supplementary/2.5_purity.html">Robustness to variable tumour purity and incorrect purity estimates</a>
                    <details>
                    <ul>
                      <li>Supplementary Figure 17. Evaluation of the peak-finding/heuristic VAF adjustment method.</li>
                      <li>Supplementary Figure 18. Supplementary Figure 18. False positive rate for positive selection (>= 1 subclone) at variable sequencing depths, tumour purities, and errors in purity estimates in 6000 synthetic tumours.</li>
                    </ul>
                    </details>
                  </li>

                </ol>
              </ol>
              
            <!-- Section 3 -->
            <hr style="border-color:rgb(0,0,0,0.2);border-width:1px;">
            
              III. Analysis in high-quality whole-genome and whole-exome sequenced tumour biopsies
              <ol>
                <ol>
                  <li>
                  <a href="./supplementary/3.1_wgs_overview.html">Application of TumE to high-quality PCAWG samples</a>
                    <details>
                    <ul>
                      <li>Supplementary Figure 19. VAF distributions with annotated TumE fits for 75 PCAWG samples with either zero or one detected subclone.</li>                  
                    </details>
                  </li>              

                </ol>
              </ol>

            <!-- Section 3 -->
            <hr style="border-color:rgb(0,0,0,0.2);border-width:1px;">
            
              IV. Transfer learning with TumE
              <ol>
                <ol>
                  <li>
                  <a href="./supplementary/4.1_transfer_learning.html">Inferring additional evolutionary parameters using an alternative simulation framework TEMULATOR</a>
                    <details>
                    <ul>
                      <li>Supplementary Figure 20. Viable fitness and emergence time parameter combinations for detectable subclones (~10 - 40% VAF) in the TEMULATOR simulation framework</li>
                      <li>Supplementary Figure 21. Comparison of predictive performance for inferring evolutionary parameters with and without pre-trained TumE models.</li>
                      <li>Supplementary Figure 22. Comparison of mean percentage error with and without post-hoc mutation rate correction.</li>
                      <li>Supplementary Figure 23. Mean percentage error for inferring parameters from TEMULATOR simulations (mutation rate, subclone cellular fraction, subclone emergence time, and subclone fitness).</li>
                    </ul>
                    </details>
                  </li>              

                </ol>
              </ol>

      </article>
      </div>
    </div>
  </div>
  </div>


        </div><p class="footer text-center"></p>
</body>
</html>
