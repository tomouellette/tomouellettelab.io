<!DOCTYPE html>
<html>
<head>
	<meta name="generator" content="Hugo 0.87.0" />

  
  <meta charset="utf-8">
  
  
  
  <title>Ouellette & Awadalla (2021)</title>
  
  <meta name="description" content="A personal website for Tom W. Ouellette">
  <meta name="author" content="Tom W. Ouellette">

  
  <meta name="viewport" content="width=device-width, initial-scale=1">

  
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Crimson+Pro&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">

  
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/skeleton.css">
  <!-- <link rel="stylesheet" href="../css/animate.css/animate.min.css"> -->

  
  <link rel="icon" type="image/png" href="https://tomouellette.github.io/images/favicon.png">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.11.1/katex.min.js"
        integrity="sha256-F/Xda58SPdcUCr+xhSGz9MA2zQBPb0ASEYKohl8UCHc=" crossorigin="anonymous">
</script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pseudocode@latest/build/pseudocode.min.css">
  <script src="https://cdn.jsdelivr.net/npm/pseudocode@latest/build/pseudocode.min.js">
  </script>
</head>
<body>

<div class="container">
  <div class="row">
    <div class="two-thirds column" style="margin-top: 10%">
      <h3 class="head-fade fadeInDown">Inferring ongoing cancer evolution from single tumour biopsies using synthetic supervised learning</h3>
      <p class="head-fade fadeInDown"><span style="font-weight:normal">Tom W. Ouellette and Philip Awadalla</span></p>
    </div>
  </div>
</div>

<div class="body-fade fadeInDown">
<div class="container">
<div class="row">
    <div class="twelve columns" style="margin-top: 0%">
      <nav id="nav" class="nav">
      
      <a class="button nav-but" href="../index.html">
       
       main
      </a> 
      
      <a class="button nav-but" href="../supplementary.html">
       
       supplementary
      </a> 
      
      <a class="button nav-but" href="../references.html">
       
       references
      </a> 
      
    </nav>
  </div>
</div>
</div>
</div>
<div id="content">
  <div class="body-fade fadeInDown">
  <div class="container">
    <div class="row">
      <div class="twelve columns intro" style="margin-top: 2%">    
        <div id = "about">
		<p style="font-size:0.85em"> <span style="font-family: Crimson Pro">
			As outlined in Methods, we implemented two alternative approaches for generating synthetic frequency distributions that recapitulate either positive selection or neutral evolution. The algorithm for simulating tumours (variant allele frequency distributions) subject to positive selection (adapted from <a href="https://www.nature.com/articles/s41588-018-0128-6">Williams et al. 2018</a>) is outlined in Algorithm 1. The algorithm for generating neutral frequency distributions is outlined in Algorithm 2 (inspired by <a href="https://www.nature.com/articles/s41588-020-0675-5">Caravagna et al. 2020</a>). The complete algorithm for the paired simulation of positively selected and neutrally evolving tumours is outlined in Algorithm 3. Software to generate synthetic tumours/VAF distributions can be found on github <a href="https://github.com/tomouellette/CanEvolve.jl">@tomouellette/CanEvolve.jl</a></span>
		</p>
	  </div>
	</div>
  </div>
</div>
<br>

<div id="content">
  <div class="body-fade fadeInDown">
      <div class="container">
        <div class= "twelve columns">
        <pre id="algorithm1" style="display:hidden; font-size: 0.6em">
            \begin{algorithm}    
            \caption{Tumours subject to positive selection (adapted from Williams et al. 2018)}
            \begin{algorithmic}
            \STATE \tiny Simulate tumour with $Q$ positively selected subclones with frequencies $>$ $L$ and $<$ U
            \WHILE {(U $<$ subclone frequency $<$ L) and (number of subclones \NOT $Q$)}
               \STATE 1. Initialize cell with $n_{clonal}$ clonal mutations
               \WHILE{current population size $<$ $N$}
                  \STATE 2. Randomly sample a cell $j$
                  \STATE 3. Draw a random number $r$ from Uniform($a$, $b$) where $a$ = 0 and $b$ = $b_{max}$ + $d_{max}$ (maximum birth and death rates of all cells in population)
                  \STATE 4. With $r$, cell $j$ will divide with probability proportional to its birth rate $b_j$ and die with a probability proportional to its death rate $d_j$
                  \IF{$b_j$ $>$ $r$}
                     \STATE 5a. Cell divides and both daughter cells acquire $k$ mutations where $k$ is Poisson distributed with mean equal to the per genome division mutation rate $\mu$          
                     \STATE 5i. Each mutation has a probability $P_d$ of being a positively selected driver and initiating a new subclone
                     \STATE 5ii. If the mutation is a driver, it is assigned a selection coefficient $s$ randomly sampled from an exponential distribution with a scale parameter 1/$\lambda$
                     \STATE 5iii. The time (in current population size $n$ divided by final population size $N$) is recorded for every mutation
                  \ELIF{$b_j$ + $d_j$  $>$ $r$ $>=$ $b_j$}
                     \STATE 5b. Cell dies
                  \ELSE
                     \STATE 5c. Nothing happens
                  \ENDIF          
               \ENDWHILE       
            \ENDWHILE
            \STATE 6. Virtual biopsy synthetic tumour and add sequencing noise
            \STATE 7. Remove mutations below hard alternate read cutoff (e.g 2 / mean sequencing depth)
            \end{algorithmic}
            \end{algorithm}
        </pre>
      </div>
    </div>
</div>

<div id="content">
  <div class="body-fade fadeInDown">
      <div class="container">
        <div class= "twelve columns">
        <pre id="algorithm2" style="display:hidden; font-size: 0.6em">
            \begin{algorithm}    
            \caption{Neutrally evolving tumours (inspired by Caravagna et al. 2020)}
            \begin{algorithmic}
            \STATE \tiny Simulate a neutral variant allele frequency distribution observed in bulk sequenced tumour populations
             \STATE 1. Randomly sample or set shape $\alpha$ and scale $\beta$ parameters for a Pareto distribution
             \STATE 2. Generate neutral 'tail' mutations by sampling $n_{non-clonal}$ mutations from Pareto($\alpha$, $\beta$)
             \STATE 3. Add $n_{clonal}$ heterozygote mutations at a frequency of 0.5
             \STATE 4. With some probability $P_{trim}$, remove variants below a randomly sampled frequency $f$ (e.g. 0.1 - 0.3) to mimic the loss of neutral tails observed in empirical samples (in general, $P_{trim}$ < 0.1)
             \STATE 5. Add sequencing noise
             \STATE 6. Remove mutations below hard alternate read cutoff (e.g 2 / mean sequencing depth)
            \end{algorithmic}
            \end{algorithm}
        </pre>
      </div>
    </div>
</div>
<div id="content">
      <div class="container">
        <div class = "twelve columns intro">            
      <object class="twelve columns" style="height: 50vh;" type="text/html" data="1.1.a_neutral_algorithm_example.html"></object>
      <p style="font-family:'Arial'; font-size: 0.6em"><b>Supplementary Figure 1.</b> An example visualization of VAF distribution generation for neutral synthetic tumours</p>
    </div>
</div>
</div>
<br>

<div id="content">
  <div class="body-fade fadeInDown">
      <div class="container">
        <div class= "twelve columns">
        <pre id="algorithm3" style="display:hidden; font-size: 0.6em">
            \begin{algorithm}    
            \caption{Paired simulation of VAF distributions from neutrally evolving and positively selected tumours}
            \begin{algorithmic}
            \STATE \tiny 1. Specify number of subclones $Q$ and minimum and maximum subclone frequencies $L$ and $U$
            \STATE 2. Randomly sample simulation parameters: $\mu$ (per genome per division mutation rate), $P_d$ (probability of driver/subclone event), $n_{clonal}$, $n_{drivers}$, $\lambda^{-1}$ (scale parameter), mean sequencing depth, $\rho$ (sequencing overdispersion parameter)
            \STATE 3. Run Algorithm 1 until synthetic data is generated with $Q$ subclones at frequencies $<$$U$ and $>$$L$
            \STATE 4. Count the approximate number of clonal $n_{clonal}$ and non-clonal $n_{non-clonal}$ mutations in the positive selection scenario
            \STATE 5. Run Algorithm 2 using $n_{clonal}$ * $\psi_{a}$ and $n_{non-clonal}$ * $\psi_{b}$  mutations. $\psi_{a}$ and $\psi_{b}$ are uniformly sampled numbers that scale the number of clonal and non-clonal mutations to capture additional heterogeneity in training sets.
            \end{algorithmic}
            \end{algorithm}
        </pre>
      </div>
    </div>
</div>

<div id="content">
      <div class="container">        
        <div class= "twelve columns intro">
          <img src="1.1.b_autosimulation.gif" width=97%>
          <p style="font-family:'Arial'; font-size: 0.6em"><b>Supplementary Figure 2.</b> A sample of 20 paired simulations with positive selection <b>(left)</b> and neutral evolution <b>(right)</b>. Orange lines indicate subclone frequencies (cellular proportion / 2)</p>
        </div>      
</div>
</div>

<script>
    pseudocode.renderElement(document.getElementById("algorithm1"));
    pseudocode.renderElement(document.getElementById("algorithm2"));
    pseudocode.renderElement(document.getElementById("algorithm3"));
</script>
</body>
</html>
